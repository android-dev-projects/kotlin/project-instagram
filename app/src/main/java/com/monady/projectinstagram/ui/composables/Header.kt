package com.monady.projectinstagram.ui.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.monady.projectinstagram.R

@Composable
fun Header(modifier: Modifier = Modifier){
    Surface(
        color = MaterialTheme.colors.primary
    ) {
        Box(
            modifier = modifier.fillMaxWidth()
        ) {
            Image(
                modifier = modifier
                    .align(Alignment.CenterStart)
                    .padding(8.dp),
                painter = painterResource(id = R.drawable.ic_logo),
                contentDescription = "Instagram Logo"
            )
            Row(
                modifier = modifier
                    .align(Alignment.CenterEnd)
            ) {
                Image(
                    modifier = modifier
                        .padding(8.dp),
                    painter = painterResource(id = R.drawable.ic_new_post),
                    contentDescription = "New Post"
                )
                Image(
                    modifier = modifier
                        .padding(8.dp),
                    painter = painterResource(id = R.drawable.ic_notifications),
                    contentDescription = "Notifications Logo"
                )
                Image(
                    modifier = modifier
                        .padding(8.dp),
                    painter = painterResource(id = R.drawable.ic_messenger),
                    contentDescription = "Messenger Logo"
                )
            }
        }
    }
}